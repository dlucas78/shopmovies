package com.dfl.shopmoviesapp.service.repository.sources.network

import com.dfl.shopmoviesapp.service.model.entity.MovieEntity
import com.google.gson.annotations.SerializedName

data class ResponsePojo(
    @SerializedName("results")
    val results: ArrayList<MovieEntity>)