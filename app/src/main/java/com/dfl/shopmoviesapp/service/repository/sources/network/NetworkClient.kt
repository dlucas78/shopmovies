package com.dfl.shopmoviesapp.service.repository.sources.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkClient {
    companion object {
        var retrofit: Retrofit? = null
        val BASE_URL = "https://api.themoviedb.org/3/"
        fun getRetrofitClient(): Retrofit? {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit
        }
    }
}