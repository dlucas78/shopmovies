package com.dfl.shopmoviesapp.service.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(
    tableName = MovieEntity.TABLE_NAME,
    primaryKeys = [MovieEntity.ID_COLUMN_NAME]
)
class MovieEntity(
    @SerializedName("id")
    @ColumnInfo(name = ID_COLUMN_NAME)
    var id: Int,
    @ColumnInfo(name = NAME_COLUMN_NAME)
    @SerializedName("title")
    var name: String,
    @ColumnInfo(name = DESCRIPTION_COLUMN_NAME)
    @SerializedName("overview")
    var description: String?,
    @ColumnInfo(name = VALUE_COLUMN_NAME)
    @SerializedName("vote_average")
    var value: Float?,
    @ColumnInfo(name = IMAGE_COLUMN_NAME)
    @SerializedName("poster_path")
    var image: String?
) {
    companion object {
        const val TABLE_NAME = "MOVIES"
        const val ID_COLUMN_NAME = "ID"
        const val NAME_COLUMN_NAME = "NAME"
        const val DESCRIPTION_COLUMN_NAME = "DESCRIPTION"
        const val VALUE_COLUMN_NAME = "VALUE"
        const val IMAGE_COLUMN_NAME = "IMAGE"
    }
}