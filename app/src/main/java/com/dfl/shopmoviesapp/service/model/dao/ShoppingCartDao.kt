package com.dfl.shopmoviesapp.service.model.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dfl.shopmoviesapp.service.model.entity.MovieEntity
import com.dfl.shopmoviesapp.service.model.entity.ShoppingCartEntity
import com.dfl.shopmoviesapp.viewModel.MovieVO

@Dao
interface ShoppingCartDao {

    @Query(
        "SELECT " + MovieEntity.ID_COLUMN_NAME + " as id, " +
                MovieEntity.NAME_COLUMN_NAME + " as name, " +
                MovieEntity.IMAGE_COLUMN_NAME + " as image, " +
                MovieEntity.VALUE_COLUMN_NAME + " as value, " +
                ShoppingCartEntity.UNITS_COLUMN_NAME + " as units " +
                " FROM " + MovieEntity.TABLE_NAME + " as mov " +
                " JOIN " + ShoppingCartEntity.TABLE_NAME + " as sho ON sho." + ShoppingCartEntity.ID_MOVIE_COLUMN_NAME + "= mov." + MovieEntity.ID_COLUMN_NAME
    )
    fun getAllShopping(): LiveData<List<MovieVO>>

    @Query(
        "SELECT " + ShoppingCartEntity.UNITS_COLUMN_NAME +
                " FROM " + ShoppingCartEntity.TABLE_NAME +
                " WHERE " + ShoppingCartEntity.ID_MOVIE_COLUMN_NAME + " =:id"
    )
    fun getUnitsShopping(id: Int): Int?

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(catalog: ShoppingCartEntity)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(catalog: ShoppingCartEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun inserts(catalog: List<ShoppingCartEntity>)

    @Query(
        "DELETE FROM " + ShoppingCartEntity.TABLE_NAME +
                " WHERE " + ShoppingCartEntity.ID_MOVIE_COLUMN_NAME + " =:id"
    )
    fun delete(id: Int)

    @Query(
        "DELETE FROM " + ShoppingCartEntity.TABLE_NAME
    )
    fun deleteAll()
}