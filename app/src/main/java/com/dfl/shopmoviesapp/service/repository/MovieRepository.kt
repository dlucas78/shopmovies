package com.dfl.shopmoviesapp.service.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.dfl.shopmoviesapp.service.model.dao.MovieDao
import com.dfl.shopmoviesapp.service.model.entity.MovieEntity

class MovieRepository(private val movieDao: MovieDao) {

    val allMovies: LiveData<List<MovieEntity>> = movieDao.getAllMovies()

    @WorkerThread
    suspend fun insert(movieEntity: MovieEntity) {
        movieDao.insert(movieEntity)
    }

    @WorkerThread
    suspend fun inserts(moviesEntity: List<MovieEntity>) {
        movieDao.inserts(moviesEntity)
    }

    fun getMovie(id: Int): MovieEntity {
        return movieDao.getMovie(id)
    }

}