package com.dfl.shopmoviesapp.service.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = ShoppingCartEntity.TABLE_NAME,
    foreignKeys = [ForeignKey(
        entity = MovieEntity::class,
        parentColumns = [MovieEntity.ID_COLUMN_NAME],
        childColumns = [ShoppingCartEntity.ID_MOVIE_COLUMN_NAME]
    )],
    primaryKeys = [ShoppingCartEntity.ID_MOVIE_COLUMN_NAME]
)
class ShoppingCartEntity(
    @ColumnInfo(name = ID_MOVIE_COLUMN_NAME)
    var idMovie: Int,
    @ColumnInfo(name = UNITS_COLUMN_NAME)
    var units: Int?
) {
    companion object {
        const val TABLE_NAME = "PURCHASES"
        const val ID_MOVIE_COLUMN_NAME = "ID_MOVIE"
        const val UNITS_COLUMN_NAME = "UNITS"
    }
}