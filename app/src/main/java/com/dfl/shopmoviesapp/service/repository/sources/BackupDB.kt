package com.dfl.shopmoviesapp.service.repository.sources

import com.dfl.shopmoviesapp.view.ui.MainActivity
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream

class BackupDB {
    /**
     * Restaura Backup de la Base de Datos
     * @param context: Recibe contexto principal
     */
    @Throws(IOException::class)
    fun deployDatabase(context: MainActivity) {
        MovieDataBase.getDatabase(context)
        MovieDataBase.INSTANCE!!.close()
        //Open your local db as the input stream
        val pathDB = context.dataDir.path + "/databases/"
        //Create the directory if it does not exist
        val directory = File(pathDB)
        val children = directory.list()
        if (!directory.exists()) {
            directory.mkdirs()
        } else {
            for (i in children.indices) {
                File(directory, children[i]).delete()
            }
        }
        val files = arrayListOf(
            "SHOPMOVIES_DB",
            "SHOPMOVIES_DB-journal"
        )

        for (nameFile in files) {
            val myInput = context.assets.open(nameFile)
            val myOutput = FileOutputStream(pathDB + nameFile)
            myInput.use { input ->
                input.copyTo(myOutput as OutputStream)
            }
            myOutput.flush()
            myOutput.close()
            myInput.close()
        }
        MovieDataBase.INSTANCE!!.isOpen
    }

}