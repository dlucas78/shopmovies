package com.dfl.shopmoviesapp.service.repository.sources.network

import com.dfl.shopmoviesapp.service.model.entity.MovieEntity
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface IMoviesApiServices {

    @GET("trending/movie/week?")
    fun getMoviesService(
        @Query("api_key") key: String,
        @Query("language") language: String = "es",
        @Query("include_image_language") lanImg: String = "es"
    ): Call<ResponsePojo>

    @GET("movie/{id}?")
    fun getMovie(
        @Path("id") id: String,
        @Query("language") language: String = "es",
        @Query("api_key") key: String
    ): Call<MovieEntity>
}