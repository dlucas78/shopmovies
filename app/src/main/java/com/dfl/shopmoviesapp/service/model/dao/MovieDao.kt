package com.dfl.shopmoviesapp.service.model.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dfl.shopmoviesapp.service.model.entity.MovieEntity

@Dao
interface MovieDao {

    @Query("SELECT * from " + MovieEntity.TABLE_NAME)
    fun getAllMovies(): LiveData<List<MovieEntity>>

    @Query(
        "SELECT * from " + MovieEntity.TABLE_NAME +
                " WHERE " + MovieEntity.ID_COLUMN_NAME + "=:id"
    )
    fun getMovie(id: Int): MovieEntity

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(catalog: MovieEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun inserts(catalog: List<MovieEntity>)

    @Query("DELETE FROM " + MovieEntity.TABLE_NAME)
    fun deleteAll()
}