package com.dfl.shopmoviesapp.service.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.dfl.shopmoviesapp.service.model.dao.ShoppingCartDao
import com.dfl.shopmoviesapp.service.model.entity.MovieEntity
import com.dfl.shopmoviesapp.service.model.entity.ShoppingCartEntity
import com.dfl.shopmoviesapp.viewModel.MovieVO

class ShoppingRepository(private val movieDao: ShoppingCartDao) {

    val allShopping: LiveData<List<MovieVO>> = movieDao.getAllShopping()

    @WorkerThread
    suspend fun insert(movieEntity: ShoppingCartEntity) {
        movieDao.insert(movieEntity)
    }

    @WorkerThread
    suspend fun update(movieEntity: ShoppingCartEntity) {
        movieDao.update(movieEntity)
    }

    @WorkerThread
    suspend fun delete(id: Int) {
        movieDao.delete(id)
    }

    @WorkerThread
    suspend fun deleteAll() {
        movieDao.deleteAll()
    }

    @WorkerThread
    suspend fun inserts(moviesEntity: List<ShoppingCartEntity>) {
        movieDao.inserts(moviesEntity)
    }


    @WorkerThread
    suspend fun addMovieUnits(movieEntity: MovieEntity) {
        var units = movieDao.getUnitsShopping(movieEntity.id)
        if (units == null)
            insert(ShoppingCartEntity(movieEntity.id, 1))
        else {
            units += 1
            update(ShoppingCartEntity(movieEntity.id, units))
        }

    }

    @WorkerThread
    suspend fun restMovieUnits(movieEntity: MovieEntity) {
        var units = movieDao.getUnitsShopping(movieEntity.id)
        if (units != null) {
            if (units == 1)
                delete(movieEntity.id)
            else {
                units -= 1
                update(ShoppingCartEntity(movieEntity.id, units))
            }
        }

    }

}