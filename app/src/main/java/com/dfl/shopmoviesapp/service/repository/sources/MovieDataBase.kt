package com.dfl.shopmoviesapp.service.repository.sources

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.dfl.shopmoviesapp.service.model.dao.MovieDao
import com.dfl.shopmoviesapp.service.model.dao.ShoppingCartDao
import com.dfl.shopmoviesapp.service.model.entity.MovieEntity
import com.dfl.shopmoviesapp.service.model.entity.ShoppingCartEntity

@Database(
    entities = [MovieEntity::class,
        ShoppingCartEntity::class],
    version = 1
)
abstract class MovieDataBase : RoomDatabase() {

    abstract fun movieDao(): MovieDao
    abstract fun shopDao(): ShoppingCartDao

    companion object {
        @Volatile
        var INSTANCE: MovieDataBase? = null

        fun getDatabase(context: Context): MovieDataBase {
            val tempInstance = INSTANCE
            if (tempInstance != null)
                return tempInstance
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MovieDataBase::class.java,
                    "SHOPMOVIES_DB"
                ).setJournalMode(JournalMode.TRUNCATE)
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}