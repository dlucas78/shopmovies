package com.dfl.shopmoviesapp.utilities

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.widget.ImageView
import android.widget.Toast
import com.dfl.shopmoviesapp.R
import com.squareup.picasso.Picasso

class Connection {

    fun getConnection(cont: Context): Boolean {

        val isConnected: Boolean
        val activeNetworkInfo: NetworkInfo? =
            (cont.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
        isConnected = activeNetworkInfo?.isConnected ?: false
        if (!isConnected)
            Toast.makeText(cont, "No hay Internet modo local!! O.O", Toast.LENGTH_LONG).show()

        return isConnected


    }
    fun setImage(chain: String?, image: ImageView, cont: Context) {
        Picasso.with(cont)
            .load(cont.getString(R.string.path_image) + chain)
            .placeholder(R.drawable.no_image_available)
            .into(image)
    }
}