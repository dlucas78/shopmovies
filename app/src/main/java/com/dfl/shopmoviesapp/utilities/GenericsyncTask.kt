package com.dfl.shopmoviesapp.utilities

import android.os.AsyncTask

class GenericsyncTask(private val handler: () -> Unit, private val handlerPost: () -> Unit) :
    AsyncTask<Void, Void, Void>() {

    override fun doInBackground(vararg params: Void?): Void? {
        handler()
        return null
    }

    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        handlerPost()
    }
}