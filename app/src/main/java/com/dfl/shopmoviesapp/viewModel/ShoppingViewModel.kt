package com.dfl.shopmoviesapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.dfl.shopmoviesapp.service.model.entity.MovieEntity
import com.dfl.shopmoviesapp.service.repository.ShoppingRepository
import com.dfl.shopmoviesapp.service.repository.sources.MovieDataBase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShoppingViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: ShoppingRepository
    var allShop: LiveData<List<MovieVO>>

    init {
        val movieDao = MovieDataBase.getDatabase(application).shopDao()
        repository = ShoppingRepository(movieDao)
        allShop = repository.allShopping
    }

    fun addMovieUnits(movies: MovieEntity) = viewModelScope.launch(Dispatchers.IO) {
        repository.addMovieUnits(movies)
    }

    fun restMovieUnits(movies: MovieEntity) = viewModelScope.launch(Dispatchers.IO) {
        repository.restMovieUnits(movies)
    }
    fun deleteAll() = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteAll()
    }


}