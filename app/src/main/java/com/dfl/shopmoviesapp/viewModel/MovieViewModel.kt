package com.dfl.shopmoviesapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.dfl.shopmoviesapp.R
import com.dfl.shopmoviesapp.service.model.entity.MovieEntity
import com.dfl.shopmoviesapp.service.repository.MovieRepository
import com.dfl.shopmoviesapp.service.repository.sources.MovieDataBase
import com.dfl.shopmoviesapp.service.repository.sources.network.IMoviesApiServices
import com.dfl.shopmoviesapp.service.repository.sources.network.NetworkClient
import com.dfl.shopmoviesapp.service.repository.sources.network.ResponsePojo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: MovieRepository
    var allMovies: LiveData<List<MovieEntity>>
    private val applicationVM: Application = application

    init {
        val movieDao = MovieDataBase.getDatabase(application).movieDao()
        repository = MovieRepository(movieDao)
        allMovies = repository.allMovies
    }

    fun insert(movie: MovieEntity) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(movie)
    }

    fun inserts(movies: List<MovieEntity>) = viewModelScope.launch(Dispatchers.IO) {
        repository.inserts(movies)
    }

    fun getMovie(id: Int): MovieEntity {
        return repository.getMovie(id)
    }

    fun getMovies() {
        val retrofit = NetworkClient.getRetrofitClient()
        val weatherAPIs = retrofit?.create(IMoviesApiServices::class.java)
        val call = weatherAPIs?.getMoviesService(applicationVM.getString(R.string.key_API_movies))

        call?.enqueue(object : Callback<ResponsePojo> {
            override fun onResponse(call: Call<ResponsePojo>, response: Response<ResponsePojo>) {
                if (response.code() == 200) {
                    if (response.body() != null)
                        inserts(response.body()!!.results)
                }
            }

            override fun onFailure(call: Call<ResponsePojo>, t: Throwable) {
                val wResponse = t.message
            }
        })
    }

    fun getMovie(id: String) {
        val retrofit = NetworkClient.getRetrofitClient()
        val weatherAPIs = retrofit?.create(IMoviesApiServices::class.java)
        val call = weatherAPIs?.getMovie(id = id, key = applicationVM.getString(R.string.key_API_movies))

        call?.enqueue(object : Callback<MovieEntity> {
            override fun onResponse(call: Call<MovieEntity>, response: Response<MovieEntity>) {
                if (response.code() == 200) {
                    if (response.body() != null)
                        insert(response.body()!!)
                }
            }

            override fun onFailure(call: Call<MovieEntity>, t: Throwable) {
                val wResponse = t.message
            }
        })
    }
}