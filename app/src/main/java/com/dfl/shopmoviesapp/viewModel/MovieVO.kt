package com.dfl.shopmoviesapp.viewModel

class MovieVO(
    var id: Int,
    var name: String,
    var image: String? = null,
    var value: Float? = null,
    var units: Int = 0
)