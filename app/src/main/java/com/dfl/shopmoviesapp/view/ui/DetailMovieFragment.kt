package com.dfl.shopmoviesapp.view.ui

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.dfl.shopmoviesapp.R
import com.dfl.shopmoviesapp.service.model.entity.MovieEntity
import com.dfl.shopmoviesapp.utilities.Connection
import com.dfl.shopmoviesapp.utilities.GenericsyncTask
import com.dfl.shopmoviesapp.viewModel.MovieViewModel
import com.dfl.shopmoviesapp.viewModel.ShoppingViewModel
import kotlinx.android.synthetic.main.fragment_detail_movie.*

class DetailMovieFragment : Fragment() {

    private lateinit var viewModelMovie: MovieViewModel
    private lateinit var viewModelShopping: ShoppingViewModel
    private var movieID: Long = 0
    private var movie: MovieEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieID = arguments!!.getLong("id")

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity!!.title = getString(R.string.detail_text)
        viewModelMovie = ViewModelProviders.of(this).get(MovieViewModel::class.java)
        viewModelShopping = ViewModelProviders.of(this).get(ShoppingViewModel::class.java)

        return inflater.inflate(R.layout.fragment_detail_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        GenericsyncTask({
            movie = viewModelMovie.getMovie(movieID.toInt())
        }, {

            if (movie == null && Connection().getConnection(context!!)) {
                viewModelMovie.getMovie(movieID.toString())
                onViewCreated(view, savedInstanceState)
                return@GenericsyncTask
            }
            textTittleDetail.text = movie?.name ?: getString(R.string.nout_found_movie)
            Connection().setImage(movie?.image, imageDetails, context!!)
            descriptionDetails.text = movie?.description
            floatingActionAdd.setOnClickListener {
                if (movie != null)
                    viewModelShopping.addMovieUnits(movie!!)
            }
            floatingActionRest.setOnClickListener {
                if (movie != null)
                    viewModelShopping.restMovieUnits(movie!!)
            }
            descriptionDetails.movementMethod = ScrollingMovementMethod()
        }).execute()
    }
}