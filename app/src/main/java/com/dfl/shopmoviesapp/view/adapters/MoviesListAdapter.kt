package com.dfl.shopmoviesapp.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.dfl.shopmoviesapp.R
import com.dfl.shopmoviesapp.service.model.entity.MovieEntity
import com.dfl.shopmoviesapp.utilities.Connection
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.text.DecimalFormat

class MoviesListAdapter(
    private var moviesList: List<MovieEntity>,
    private var context: Context,
    private var mClickListener: BtnClickListener? = null
) : BaseAdapter() {

    interface BtnClickListener {
        fun onBtnClick(identifier: Long)
        fun onBtnClickAdd(movie: MovieEntity)
        fun onBtnClickRest(movie: MovieEntity)
    }

    override fun getCount() = moviesList.size
    override fun getItem(position: Int) = moviesList[position]
    override fun getItemId(position: Int) = moviesList[position].id.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(context)
        var view = convertView
        if (view == null)
            view = layoutInflater.inflate(R.layout.adapter_movies_list, parent, false)

        val text = view!!.findViewById<TextView>(R.id.nameFilm)
        val price = view.findViewById<TextView>(R.id.priceValue)
        val details = view.findViewById<FloatingActionButton>(R.id.floatingDetailsMovie)
        val addMovie = view.findViewById<FloatingActionButton>(R.id.floatingAddMovie)
        val restMovie = view.findViewById<FloatingActionButton>(R.id.floatingSubtractMovie)
        val image = view.findViewById<ImageView>(R.id.imageFilm)
        details.tag = getItemId(position)
        addMovie.tag = getItem(position)
        restMovie.tag = getItem(position)
        text.text = getItem(position).name

        price.text = setValue(getItem(position).value!!)

        Connection().setImage(getItem(position).image!!, image, context)
        details.setOnClickListener { v ->
            if (mClickListener != null)
                mClickListener!!.onBtnClick(v.tag as Long)
        }
        addMovie.setOnClickListener { v ->
            if (mClickListener != null)
                mClickListener!!.onBtnClickAdd(v.tag as MovieEntity)
        }
        restMovie.setOnClickListener { v ->
            if (mClickListener != null)
                mClickListener!!.onBtnClickRest(v.tag as MovieEntity)
        }
        return view
    }

    private fun setValue(number: Float): String {
        val change = number * 3000
        return DecimalFormat("#.##").format(change).toString()
    }

}