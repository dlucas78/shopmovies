package com.dfl.shopmoviesapp.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.dfl.shopmoviesapp.R
import com.dfl.shopmoviesapp.view.adapters.ShoppingListAdapter
import com.dfl.shopmoviesapp.viewModel.MovieVO
import com.dfl.shopmoviesapp.viewModel.MovieViewModel
import com.dfl.shopmoviesapp.viewModel.ShoppingViewModel
import kotlinx.android.synthetic.main.fragment_shopping_cart.*
import java.text.DecimalFormat

class ShoppingCartFragment : Fragment() {
    lateinit var navController: NavController

    private lateinit var viewModelMovie: MovieViewModel
    private lateinit var viewModelShopping: ShoppingViewModel
    private var arrayshop: List<MovieVO>? = ArrayList()
    private var totalPrice = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity!!.title = getString(R.string.tittle_cart_shopping)

        viewModelMovie = ViewModelProviders.of(this).get(MovieViewModel::class.java)
        viewModelShopping = ViewModelProviders.of(this).get(ShoppingViewModel::class.java)
        viewModelShopping.allShop.observe(this, Observer { words ->
            words?.let {
                totalPrice = 0
                totalPriceText.text = getString(R.string.total_init)
                arrayshop = it
                generateAdapter()
            }
        })

        return inflater.inflate(R.layout.fragment_shopping_cart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        buttonClearAll.setOnClickListener {
            viewModelShopping.deleteAll()
        }
    }

    private fun generateAdapter() {
        arrayshop!!.forEach {
            totalPrice += setValue(it.value!!, it.units)
        }
        totalPriceText.text = getString(R.string.total) + totalPrice
        val adapterMovie = ShoppingListAdapter(arrayshop!!, context!!, object : ShoppingListAdapter.BtnClickListener {

            override fun onBtnClick(identifier: Long) {
                val bundle = bundleOf("id" to identifier)
                navController.navigate(
                    R.id.action_shoppingCartFragment_to_detailMovieFragment,
                    bundle
                )
            }
        })
        listShopwMovies.adapter = adapterMovie
    }

    private fun setValue(number: Float, count: Int): Int {
        val change = number * 3000 * count
        return DecimalFormat("#.##").format(change).toInt()
    }

}