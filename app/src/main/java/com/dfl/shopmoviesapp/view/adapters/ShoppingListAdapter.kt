package com.dfl.shopmoviesapp.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.dfl.shopmoviesapp.R
import com.dfl.shopmoviesapp.utilities.Connection
import com.dfl.shopmoviesapp.viewModel.MovieVO
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.text.DecimalFormat

class ShoppingListAdapter(
    private var shoppingList: List<MovieVO>,
    private var context: Context,
    private var mClickListener: BtnClickListener? = null
) : BaseAdapter() {

    interface BtnClickListener {
        fun onBtnClick(identifier: Long)
    }

    override fun getCount() = shoppingList.size
    override fun getItem(position: Int) = shoppingList[position]

    override fun getItemId(position: Int) = shoppingList[position].id.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val layoutInflater: LayoutInflater = LayoutInflater.from(context)
        var view = convertView
        if (view == null)
            view = layoutInflater.inflate(R.layout.adapter_shopping_list, parent, false)

        val units = view!!.findViewById<TextView>(R.id.unitsFilmShop)
        val price = view.findViewById<TextView>(R.id.priceShop)
        val title = view.findViewById<TextView>(R.id.nameFilmShop)
        val details = view.findViewById<FloatingActionButton>(R.id.detailShop)
        val image = view.findViewById<ImageView>(R.id.imageFilmShop)

        Connection().setImage(getItem(position).image!!, image, context)
        details.tag = getItemId(position)
        units.text = getItem(position).units.toString()


        price.text = setValue(getItem(position).value!!, getItem(position).units)
        title.text = getItem(position).name

        details.setOnClickListener { v ->
            if (mClickListener != null)
                mClickListener!!.onBtnClick(v.tag as Long)
        }
        return view
    }


    private fun setValue(number: Float, count: Int): String {
        val change = number * 3000 * count
        return DecimalFormat("#.##").format(change).toString()
    }

}