package com.dfl.shopmoviesapp.view.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dfl.shopmoviesapp.R
import com.dfl.shopmoviesapp.service.repository.sources.BackupDB
import com.facebook.stetho.Stetho

class MainActivity : AppCompatActivity() {
    private val requestViewModel by lazy { BackupDB() }
    private val sp by lazy { getSharedPreferences(getString(R.string.sharedpreferences_name), 0) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Stetho.initializeWithDefaults(this)
        if (sp.getInt(getString(R.string.first_install), 0) == 0) {
            requestViewModel.deployDatabase(this)
            sp.edit().putInt(getString(R.string.first_install), 1).apply()
        }
    }

}
