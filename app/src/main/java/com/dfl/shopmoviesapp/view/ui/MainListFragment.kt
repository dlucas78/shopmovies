package com.dfl.shopmoviesapp.view.ui

import android.os.Bundle
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.dfl.shopmoviesapp.R
import com.dfl.shopmoviesapp.service.model.entity.MovieEntity
import com.dfl.shopmoviesapp.utilities.Connection
import com.dfl.shopmoviesapp.view.adapters.MoviesListAdapter
import com.dfl.shopmoviesapp.viewModel.MovieViewModel
import com.dfl.shopmoviesapp.viewModel.ShoppingViewModel
import kotlinx.android.synthetic.main.fragment_main_list.*

class MainListFragment : Fragment(), View.OnClickListener {
    lateinit var navController: NavController

    private lateinit var viewModelMovie: MovieViewModel
    private lateinit var viewModelShopping: ShoppingViewModel
    private var arrayMovi: List<MovieEntity>? = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        viewModelMovie = ViewModelProviders.of(this).get(MovieViewModel::class.java)
        viewModelShopping = ViewModelProviders.of(this).get(ShoppingViewModel::class.java)
        if (Connection().getConnection(context!!))
            viewModelMovie.getMovies()
        activity!!.title = getString(R.string.title_movies)
        viewModelMovie.allMovies.observe(this, Observer { words ->
            words?.let {
                arrayMovi = it
                generateAdapter()
            }
        })

        return inflater.inflate(R.layout.fragment_main_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        generateAdapter()
    }

    private fun generateAdapter() {
        val adapterMovie = MoviesListAdapter(arrayMovi!!, context!!, object : MoviesListAdapter.BtnClickListener {
            override fun onBtnClickAdd(movie: MovieEntity) {
                viewModelShopping.addMovieUnits(movie)
            }

            override fun onBtnClickRest(movie: MovieEntity) {
                viewModelShopping.restMovieUnits(movie)
            }

            override fun onBtnClick(identifier: Long) {
                val bundle = bundleOf("id" to identifier)
                navController.navigate(
                    R.id.action_mainListFragment_to_detailMovieFragment,
                    bundle
                )
            }
        })
        listViewMovies.adapter = adapterMovie
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_app, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.icShopping)
            navController.navigate(R.id.action_mainListFragment_to_shoppingCartFragment)
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {

    }


}